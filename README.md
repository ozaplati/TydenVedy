# Týden Vědy na Jaderce
Tento kod byl vyvinut pro popularizaci částicové fyziky v rámci projektu Týden Vědy na Jaderce 2024 organizovaný Fakultou Jadernou a Fyzikálně Inženýrskou (FJFI). Tento balíček obsahuje jednoduchou Monte-Carlo (MC) analýzu pro $Z\rightarrow \mu^+ \mu^-$ pomocí MC Herwig7 a programů ROOT a Rivet.

# INSTRUKCE
## Login
### Login z prostředí se operačním systémem Windows
- Pokud Tvůj PC/laptop běží na operačním systému Windows, je třeba nejdříve nainstalovat terminál emulátor pro X Windows systém, například `XTerm` program viz:
   - https://invisible-island.net/xterm/#download
- Po instalaci programu `XTerm` lze spustit prostředí terminálu, ze kterého se připojíme na vzdálený výpočetní klastr `sunrise` jako uživatel `guest1` pomocí přikazu `ssh`:
```
ssh -XY guest1@sunrise.fjfi.cvut.cz
```

### Login z prostředí s operačním systému Linux a iOS
- Pokud Tvůj PC/laptop běží na operačním systému Linux nebo iOS, můžete se na výpočetní klastr `sunrise` přihlásit jako uživatel `guest1` pomocí příkazu `ssh` rovnou:
```
ssh -XY guest1@sunrise.fjfi.cvut.cz
```
 
## Stažení kodu
- Vytvoř si svůj nový pracovní adresář např. `TestArea`, kam si stáhneme analyzační kod z `git`u
```
mkdir TestArea
git clone https://gitlab.cern.ch/ozaplati/TydenVedy.git
cd TydenVedy
```

## Setup prostředí - `Root` a `Herwig`
- Vždy je potřeba nastavit pracovní prostředí, typicky definovat systemové proměné a cesty, cesty k programům, ktere chceme použit a podobně.
```
cd Generate
source setup_env.sh
```

## Kompilace třídy `Browser`
- Zkompiluj třídu `Browser` společně s `Herwig`em. Kompilace a linkování je založena na tzv. `Makefile` souboru, pro kompilaci a linkovaní Ti postačí příkaz `make`
```
make
```

## Generování událostí s `Herwig`em
- Konfigurace pro `Herwig` je definována v tzv. in-kartě `LHC-all.in`,  lze zde např. najít:
   - p+p srážky
   - těžišťová energie 13 TeV
   - maticový element jako `MEqq2gZ2ff`
   - includovaní třídy Browser, resp. `Browser.so`
   - includovaní Rivet Analýzy, např. `ATLAS_2017_I1514251`
   - nastavení pro p+p Event Display:
   - a mnoho dalších ...
- `Hewig` in-kartu načtes jako:
```
Herwig read LHC-all.in
```
- Nyní můžes vygenerovat Tvoje první události s `Herwig`em; parameter `N` je počet událostí, parametr `s` je tzv. seed
```
Herwig run  LHC-all.run -N 20000 -s 12385
```


## Analyzační kod - kompilace
- Jdi do složky `../Analysis` a zkompiluj kod pro `analyza.cpp`, který používá `Root`, `FastJet` a `TreeReader`
```
cd ../Analysis
source setup.sh
source compile.sh
```

## Analyzační kod - spuštění
- Předtím než spustíš analyzační kod, zkopíruj si vygenerovaný `HERWIG_data.root` soubor do složky `Analysis/input`
```
cp ../Generate/HERWIG_data.root input/.
```
- Pak můžeš spustit analyzační kod
```
./Analysis
```
- Výstupní root-soubor z `Analysis` programu najdeš ve složce `output/histograms.root`



## Extrakce hmotnosti, rozpadové šířky a poločasu života $Z$ bosomu
- Podivej se na skript `Fit.C`, který načítá root-soubor `output/histograms.root` a v něm histogram pro hmotnost di-leptonového páru (resp. spektrum hmototnosti $Z$ bosonu), kterou následně prokládá Gaussovou a Breight-Wiegnerobou distribucí. Z fitů pak lze určit invariantní hmotnost $Z$ bosonu pomocí parametrů $\mu$, resp. $x_0$, rozpadovou šířku $\Gamma$ a případně i střední dobu života jako $\tau_\text{Z}=1/\Gamma$
 - Gaussova distribuce:
$$ \text{Gauss}(x; A, \mu, \sigma) = A \exp \left( -\frac{(x-\mu)^2}{2\sigma^2} \right) $$
 - Bright-Wignerova distribuce:
$$ \text{Breight-Wiegner}(x; A, \Gamma, x_0) = A \frac{ \frac{\Gamma}{2} }{ \left( \frac{\Gamma}{2} \right) ^2 + \left( x-x_0 \right) ^2 } $$
- Skript `Fit.C` lze spustit jako
```
root -l Fit.C
```

# Bonus
## Event display z p+p srážky s `Herwig7`
- Podívej se do in-karty `LHC-all.in`
- Odkomentuj následující "Analyses" blok, který Ti navíc vykreslí `42` událost během generovaní událostí
```
    ##################################################
    ## Analyses
    ##################################################
    cd /Herwig/Generators
    set /Herwig/Analysis/Plot:EventNumber 42
    insert EventGenerator:AnalysisHandlers 0 /Herwig/Analysis/Plot
```
- Znovu načti in-kartu `LHC-all.in`
```
Herwig read LHC-all.in
```
- Nageneruj alespoň 42 událostí
```
Herwig run LHC-all.run -N 50 -s 12385
```
- Ve složce nyní nalezneš nový soubor `LHC-all-S12385-Plot-42.dot`. Překonvertuj `.dot` soubor do `.svg`, nebo `.png`:
```
dot -Tsvg ./LHC-all-S12385-Plot-42.dot > plot.svg
dot -Tpng ./LHC-all-S12385-Plot-42.dot > plot.png
```
- Event display z p+p srážky v podobě `plot.svg`, nebo `plot.png`, si můžeš prohlédnout např. v programu `eog`, nebo `display`
```
eog plot.png &
display plot.png &
```
- Dalším krokem bude zopakovat celou proceduru, nyní ale s vypnutím efektů hadronizace, underlying events, ... v in-kartě `LHC-all.in`. Odkomentuj tedy následující řádky
```
set /Herwig/EventHandlers/EventHandler:HadronizationHandler NULL
```
- Pokud si chceš obrázek stáhnout/zkopírovat z výpočetního klastru na Tvůj PC/laptop, můžeš. Otevři si u sebe nový terminál a zadej následující příkaz. Je však třeba zadat správné cesty ke vstupnímu/výstupnímu souboru odkud/kam
```
scp guest1@sunrise.fjfi.cvut.cz:/DOPLN/VSTUPNI/CESTU/K/SOUBORU/NA/KLASTRU/ODKUD /DOPLN/VYSTUPNI/CESTU/K/SOUBORU/NA/TVEM/PC/KAM
```
- Do aktuálního adresáře u Tebe na PC/laptopu si můžeš stáhnout všechny `.png` soubory jako
```
scp guest1@sunrise.fjfi.cvut.cz:/DOPLN/VSTUPNI/CESTU/K/SOUBORU/NA/KLASTRU/ODKU/*.png .
```



## Rivet analysis
- Zvol si Tvoji oblíbenou `Rivet` analýzu ze https://rivet.hepforge.org/analyses/
- Já si např. vybral `ATLAS_2017_I1514251`
- Odkomentuj následující "Rivet analysis" blok v in-kartě `LHC-all.in`, pripadně přidej Tvoji oblíbebou `Rivet` analýzu
```
   cd /Herwig/Generators
   create ThePEG::RivetAnalysis /Herwig/Analysis/RivetAnalysis RivetAnalysis.so
   insert EventGenerator:AnalysisHandlers 0 /Herwig/Analysis/RivetAnalysis
   insert /Herwig/Analysis/RivetAnalysis:Analyses 0 ATLAS_2017_I1514251
```
- Znovu načti in-kartu `LHC-all.in`
```
  Herwig read LHC-all.in
```
- Nageneruj události
```
  Herwig run LHC-all.run -N 1000 -s 12385
```
- Nyní v aktuálním adresáři najdeš nový soubor `LHC-all-S12385.yoda`, zkonvertuj `.yoda` sobor do `.html`
```
  rivet-mkhtml LHC-all-S12385.yoda
```
 - Pro konverzi z `.yoda` do `.html` souboru pomocí příkazu `rivet-mkhtml` bude možná třeba setup pro novější verzi `Rivet`u, pokud je to Tvůj případ, otevři si nový terminál, zaloguj se a setupuj prostření s novější verzí `Rivet`u pomocí:
```
  source /data2/vyuka/szd/setup_pythia-rivet.sh
```
- Podivej se na vystupy např. ve `Firefox`u
```
  firefox rivet-plots/index.html &
```

## Užitečné odkazy
- Herwig 7:
   - https://herwig.hepforge.org/tutorials/hardprocess/builtin.html
   - https://herwig.hepforge.org/doxygen/MEqq2gZ2ffInterfaces.html
- Rivet
   - https://rivet.hepforge.org
   - https://rivet.hepforge.org/analyses
- Particle identification codes:
   - https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf
   - https://pdg.lbl.gov/2020/reviews/rpp2020-rev-monte-carlo-numbering.pdf
- ThePEG - Particle Data Class
   - https://thepeg.hepforge.org/doxygen/classThePEG_1_1ParticleData.html
- ROOT:
    - Zaklady:
         - https://root.cern.ch/root/htmldoc/guides/primer/ROOTPrimer.html
    - Fitovaní:
         - https://root.cern.ch/root/htmldoc/guides/users-guide/FittingHistograms.html
    - Grafika:
         - https://root.cern.ch/doc/master/classTColor.html
         - https://root.cern/root/html532/TAttLine.html
         - https://root.cern.ch/doc/master/classTAttMarker.html
         - https://root.cern/manual/graphics/
         - https://root.cern/doc/master/group__tutorial__graphics.html
         - https://root.cern.ch/root/html528/TLatex.html
- XTerm
    - https://invisible-island.net/xterm/#download
- Putty
    - https://www.putty.org/
    - https://www.ssh.com/academy/ssh/putty/windows
