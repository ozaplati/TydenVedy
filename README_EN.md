# Week of Science at FNSPE
This code was developed for the Popularisation of Particle Physics within the Week of Science 2024 organised by the Faculty of Nuclear Sciences and Physical Engineering (FNSPE). This package includes a simple $Z\rightarrow \mu^+ \mu^-$ analysis based on Monte-Carlo Herwig7 using Root and Rivet programs.
 
# INSTRUCTIONS

## Login
### Login from Windows system
- If your PC/laptop has Windows system, you should install terminal emulator for the X Windows System first, for example XTerm program from
 - https://invisible-island.net/xterm/#download
- When you install the XTerm, then you can open your terminal section login to the sunrise cluster as `quest1` using `ssh` command
```
ssh -XY guest1@sunrise.fjfi.cvut.cz
```

### Login from Linux and iOS systems
- If your PC/laptop has Linux or iOS system, you can login to the sunrise cluster as `quest1` from terminal directly
```
ssh -XY guest1@sunrise.fjfi.cvut.cz
```

 
## Get the code
- Create your `TestArea` directory and download the code from `git`
``` 
mkdir TestArea
git clone https://gitlab.cern.ch/ozaplati/TydenVedy.git
cd TydenVedy
```

## Setup environment - Root and `Herwig`
```
cd Generate
source setup_env.sh	
```

## Compilation - `Browser` class
- Compilation of `Browser` class with `Herwig` using `Makefile`
```
make
```

## Run `Herwig`
- The configuration of `Herwig` is saved in the input card `LHC-all.in`- by now with:
 - proton-proton collisions
 - centre-of-mass energy 13 TeV
 - ...
```
Herwig read LHC-all.in 
```
- Generate events with `Herwig`; parameter `N` is the number of events, parameter `s` is a seed
```
Herwig run  LHC-all.run -N 20000 -s12385 
```


## Compile analyza.cpp
- Go to the `../Analysis` directory and compile `analyza.cpp` using `Root`, `FastJet` and `TreeReader`
 - Note: YOU HAVE TO REWRITE THE INPUT FILE IN SOURCE CODE-TreeReader.h
```
cd ../Analysis
source setup.sh
source compile.sh
```

## Run the analysis
- Before you run the `Analysis`, copy your `HERWIG_data.root` file
- Then you can execute `./Analysis`
```
cp ../Generate/HERWIG_data.root .
./Analysis
```
- Output root-file from the `Analysis` is available in `output/histograms.root`


## Fit mass distribution in `Root` terminal
- Have a look in `Fit.C`
- Read `output/histograms.root` file in `Fit.C` macro and try to fit a di-lepton mass distribution of Z-boson yourself using:
 - Gaussian distribution:
$$ Gauss(x; A, \mu, \sigma) = A \exp \left( -\frac{(x-\mu)^2}{2\sigma^2} \right) $$
 - Bright-Wigner distribution:
$$ Breight-Wiegner(x; A, \Gamma, x_0) = A \frac{ \frac{\Gamma}{2} }{ \left( \frac{\Gamma}{2} \right) ^2 + \left( x-x_0 \right) ^2 } $$
- Evaluate the Z-boson mass $m_\text{Z}$ from the fit
- Run the macro `Fit.C` as
```
root -l Fit.C
```

# More fun with `Herwig`
## Display the collision with `Herwig`
- Have a look on the input card `LHC-all.in`
- Uncomment "Analyses" block of
```
	##################################################
	## Analyses
	##################################################
	cd /Herwig/Generators
	set /Herwig/Analysis/Plot:EventNumber 42
	insert EventGenerator:AnalysisHandlers 0 /Herwig/Analysis/Plot
```
- load `LHC-all.in` again  as
```
Herwig read LHC-all.in
```
- Generate at least 42 events as
```
Herwig run LHC-all.run -N 50 -s12385 
```
- Now you can find a new file of `LHC-all-S12385-Plot-42.dot`, convert the `.dot` file to `.svg` or `.png` as:
```
dot -Tsvg ./LHC-all-S12385-Plot-42.dot > plot.svg
dot -Tpng ./LHC-all-S12385-Plot-42.dot > plot.png
```
- Have a look on  the `plot.svg` and `plot.png` files with your favourite editor
```
eog plot.png
display plot.png
```
- Repeate the same steps without hadronization applied - switch off hadronozation in `LHC-all.in`, uncomment the line below only in `LHC-all.in`
```
set /Herwig/EventHandlers/EventHandler:HadronizationHandler NULL
```
- If you want download `*png` file you can use `scp` command

```
scp guest1@sunrise.fjfi.cvut.cz:/<YOUR/PARH/TO/GIVEN/FILE/> <YOUR/PATH/AT/YOUR/COMPUTER>
```
- Or to copy the file to your current working directory
```
scp guest1@sunrise.fjfi.cvut.cz:/<YOUR/PARH/TO/GIVEN/FILE/> .
```

## Rivet analysis
- Have a look on your favorite `Rivet Analysis` on https://rivet.hepforge.org/analyses/
- Uncomment following "Rivet analysis" block in `LHC-all.in`
```
   cd /Herwig/Generators
   create ThePEG::RivetAnalysis /Herwig/Analysis/RivetAnalysis RivetAnalysis.so
   insert EventGenerator:AnalysisHandlers 0 /Herwig/Analysis/RivetAnalysis
   insert /Herwig/Analysis/RivetAnalysis:Analyses 0 ATLAS_2017_I1514251 
```

- load the input card `LHC-all.in` as
```
  Herwig read LHC-all.in
```
- Generate events as
```
  Herwig run LHC-all.run -N 1000 -s12385 
```
- Now you can find a new file `LHC-all-S12385.yoda`, convert the `.yoda` file to `.html`
```
  rivet-mkhtml LHC-all-S12385.yoda
```
 - If you need an updated version of rivet, open a new terminal section and setup higher rivet version as
```
  source /data2/vyuka/szd/setup_pythia-rivet.sh
```
- Have a look on the plots in `Firefox`
```
  firefox rivet-plots/index.html
```

## Some useful links
- Herwig 7:
   - https://herwig.hepforge.org/tutorials/hardprocess/builtin.html
   - https://herwig.hepforge.org/doxygen/MEqq2gZ2ffInterfaces.html
- Rivet
   - https://rivet.hepforge.org
   - https://rivet.hepforge.org/analyses
- Particle identification codes: 
   - https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf
   - https://pdg.lbl.gov/2020/reviews/rpp2020-rev-monte-carlo-numbering.pdf
- ThePEG - Particle Data Class
   - https://thepeg.hepforge.org/doxygen/classThePEG_1_1ParticleData.html
- ROOT:
    - Basics:
         - https://root.cern.ch/root/htmldoc/guides/primer/ROOTPrimer.html
    - Fit: 
         - https://root.cern.ch/root/htmldoc/guides/users-guide/FittingHistograms.html
    - Graphic:
         - https://root.cern.ch/doc/master/classTColor.html
         - https://root.cern/root/html532/TAttLine.html
         - https://root.cern.ch/doc/master/classTAttMarker.html
         - https://root.cern/manual/graphics/
         - https://root.cern/doc/master/group__tutorial__graphics.html
         - https://root.cern.ch/root/html528/TLatex.html
- Putty
    - Note, with Putty you can run scp
    - https://www.putty.org/
    - https://www.ssh.com/academy/ssh/putty/windows
