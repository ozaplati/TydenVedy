{
	/// gROOT a gStyle
	gROOT->ForceStyle();
	gStyle->SetOptStat(0);
	
	//gStyle->SetOptFit(1111);
	//p = 1 print probability
	//c = 1 print Chi-square/number of degrees of freedom
	//e = 1 print errors (if e=1, v must be 1)
	//v = 1 print name/values of parameters
	
	/// Nacti root-file, nacti histgram
	TFile f("output/histograms.root");
	TH1D *h1 = (TH1D*)f.Get("Pair_m");
	      h1->SetTitle("");
	      /// Grafika
	      h1->SetMarkerStyle(20);
	      h1->SetMarkerSize(1.0);
	      h1->SetMarkerColor(2);
	      h1->SetLineWidth(2.0);
	      /// Popisky
	      h1->SetXTitle("m_{Z} [GeV/c^{2}]"); 
	      h1->SetYTitle("N_{events} [-]"); 
	      /// Zoom
	      int N_events = h1->GetEntries();
	      h1->GetYaxis()->SetRangeUser(0., N_events/3.0); 
	      h1->GetXaxis()->SetRangeUser(86.,96.);

	double x_min = 80.0;
	double x_max = 100.0;
	
	// Gaussian fit
	//TF1 * fit_Gauss = new TF1("Gauss", "[0]*exp(-0.5*((x-[1])/[2])^2", x_min, x_max);
	TF1 * fit_Gauss = new TF1("Gauss", "gaus", x_min, x_max);
	      fit_Gauss->SetParameters( N_events/4.0, 91.0, 2.5);
	      fit_Gauss->SetLineColor(kRed);
	      fit_Gauss->SetLineWidth(3);
	
	/// Breit-Wigner fit
	TF1 * fit_BW = new TF1("BreitWigner","[0]*([1]*[1]/((x-[2])*(x-[2])+([1]/2.)*([1]/2.)))", x_min, x_max);
	     fit_BW->SetParameters(600.,2.5,91.2);
	    /// Ruzne blbustky:
	    //fit_BW->FixParameter(2,90.);
	    //fit_BW->SetParLimits(750,2.0,90.);
	    //fit_BW->FixParameter(2,90.);
	    /// Grafika
	    fit_BW->SetLineColor(kBlue);
	    fit_BW->SetLineWidth(3);
	
	/// Canvas
	TCanvas c("c","c",900,600);
	
	/// Fituj s Gaussem
	h1->Fit("Gauss","IML","", x_min, x_max);
		 
	/// Fituj s Breight-Wignerem
	h1->Fit("BreitWigner","IML","", x_min, x_max);
	
	cout << "\n\n\n" << endl;
	cout << "=============================================================" << endl;
	cout << "Fit - Bright-Wigner" << endl;
	cout << "\t NDF:                     " << fit_BW->GetNDF() << endl;
	cout << "\t N points:                " << fit_BW->GetNumberFitPoints() << endl;
	cout << "\t N free parameters (NDF): " << fit_BW->GetNumberFreeParameters() << endl;
	cout << "\t Chi2:                    " << fit_BW->GetChisquare() << endl;
	cout << "\t Chi2/NDF:                " << fit_BW->GetChisquare() / fit_BW->GetNumberFreeParameters() << endl;
	cout << "\t p0 as Amplitude:         " << fit_BW->GetParameter(0) << " +-" << fit_BW->GetParError(0) << endl;
	cout << "\t p1 as Gamma:             " << fit_BW->GetParameter(1) << " +-" << fit_BW->GetParError(1) << endl;	
	cout << "\t p2 as x_0:               " << fit_BW->GetParameter(2) << " +-" << fit_BW->GetParError(2) << endl;
	cout << "=============================================================" << endl;
	cout << "\n\n\n" << endl;
	cout << "=============================================================" << endl;
	cout << "Fit - Gauss" << endl;
	cout << "\t NDF:                     " << fit_Gauss->GetNDF() << endl;
	cout << "\t N points:                " << fit_Gauss->GetNumberFitPoints() << endl;
	cout << "\t N free parameters (NDF): " << fit_Gauss->GetNumberFreeParameters() << endl;
	cout << "\t Chi2:                    " << fit_Gauss->GetChisquare() << endl;
	cout << "\t Chi2/NDF:                " << fit_Gauss->GetChisquare() / fit_Gauss->GetNumberFreeParameters() << endl;
	cout << "\t p0 as Amplitude:         " << fit_Gauss->GetParameter(0) << " +-" << fit_Gauss->GetParError(0) << endl;
	cout << "\t p1 as mu:                " << fit_Gauss->GetParameter(1) << " +-" << fit_Gauss->GetParError(1) << endl;	
	cout << "\t p2 as sigma:             " << fit_Gauss->GetParameter(2) << " +-" << fit_Gauss->GetParError(2) << endl;
	cout << "=============================================================" << endl;
			
	h1->Draw("PE0");
	fit_BW->Draw("same");
	fit_Gauss->Draw("same");

	TLegend * leg = new TLegend(0.7, 0.7, 0.9, 0.9);
	          leg->AddEntry(h1,        "Monte Carlo Herwig7");
	          leg->AddEntry(fit_BW,    "Fit - Breit-Wigner");
	          leg->AddEntry(fit_Gauss, "Fit - Gauss");
	          leg->Draw("same");

	/// Popisky - TLatex
	TLatex latex;
	latex.SetTextAlign(13);
	latex.SetTextSize(0.055);
	latex.DrawLatexNDC(.15,.88,"Monte Carlo simulace");
	latex.DrawLatexNDC(.15,.83,"Herwig 7");
	latex.DrawLatexNDC(.15,.78,"pp#rightarrowZ#rightarrow#mu^{+}#mu^{-}, 13 TeV");
	
	/// Ulozit canvas jako .png .eps
	c.SaveAs("Pair_m.png");
	c.SaveAs("Pair_m.eps");
}





