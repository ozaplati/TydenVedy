
# mkdir
mkdir -p build
mkdir -p input
mkdir -p output

# exports
export ANALYSIS=`pwd`
export ANALYSIS_BUILD=${ANALYSIS}/build
export ANALYSIS_INPUT=${ANALYSIS}/input
export ANALYSIS_OUTPUT=${ANALYSIS}/output

# echo
echo ""
echo "exported paths:"
echo "ANALYSIS:          ${ANALYSIS}"
echo "ANALYSIS_BUILD:    ${ANALYSIS_BUILD}"
echo "ANALYSIS_OUTPUT:   ${ANALYSIS_INPUT}"
echo "ANALYSIS_OUTPUT:   ${ANALYSIS_OUTPUT}"


# Setup CMake for RooUnfold package
# - default Cmake version of 2.8 do not works
echo ""
echo "SetupAtlas"
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

setupATLAS
echo ""
echo "Setup CMake"
asetup none,cmakesetup --cmakeversion=3.14.7


# Setup Root
echo ""
echo "Setup ROOT"
. /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.18.02/x86_64-centos7-gcc48-opt/bin/thisroot.sh


echo ""
echo "DONE"
