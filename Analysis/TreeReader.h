//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sat Sep 15 09:56:45 2018 by ROOT version 6.08/06
// from TTree strom/strom
// found on file: HERWIG_data.root
//////////////////////////////////////////////////////////

#ifndef TreeReader_h
#define TreeReader_h


#include "TTree.h"
#include <TH2.h>
#include "TH1.h"
#include <TStyle.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TVectorF.h"
#include "TLorentzVector.h"

#include <sstream>  
#include <vector> 
#include <cstdio>

#include <string>
#include <math.h>
#include <iostream>


using namespace std;


class TreeReader {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Double_t        event_weight;
   Double_t        Xsec;
   vector<double>  *px;		// vectory pro ukladani nasich dat ... neco jako C++ array, ale mnohem pokrocilejsi objekt - pripadne se opteje 
   vector<double>  *py;         // hybnosti v ose x, y a z
   vector<double>  *pz;
   vector<double>  *e;		// energie
   vector<int>     *id;		// index castice v Herwigu

   // List of branches - V Branches jsou ulozene data z rootFilu ktery jsem ziskali po nagerovani udalosti v Herwigu
   // Branche umoznuji propojit ruzne veliciny o jedne castici/objektu v udalosti - pro ulozeni informaci o cele udalostu je to mnohem kompaktnejsi objekt 
   // nez pouhe pole, pole poli ci vektoru...
   TBranch        *b_event_weight;   //!
   TBranch        *b_Xsec;   //!
   TBranch        *b_px;   //!
   TBranch        *b_py;   //!
   TBranch        *b_pz;   //!
   TBranch        *b_e;   //!
   TBranch        *b_id;   //!

   // Objekt pro nacitani TTree
   TreeReader(TTree *tree=0);
   // destruktor teto tridy TreeReader - automaticke mazani objektu, kdyz chci (program chce) smazat objet prestal v pameti existovat
   virtual ~TreeReader();
   virtual Int_t    Cut(Long64_t entry);	// vyberova kriteria
   virtual Int_t    GetEntry(Long64_t entry);	// ziskej pocet eventu v TTree
   virtual Long64_t LoadTree(Long64_t entry);	// nacti TTree
   virtual void     Init(TTree *tree);		// inicializuj "nasi" tridu TreeReader
   virtual void     Loop();			// metoda - v kazdem eventu udelej to a ono
   virtual Bool_t   Notify();			// nyni nezucerlna metoda, generuje se autmaticky
   virtual void     Show(Long64_t entry = -1);	// kontrolni metoda - ukaz
};

#endif

#ifdef TreeReader_cxx
TreeReader::TreeReader(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("input/HERWIG_data.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("input/HERWIG_data.root");
      }
      f->GetObject("strom",tree);

   }
   Init(tree);
}

TreeReader::~TreeReader()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t TreeReader::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t TreeReader::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void TreeReader::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   px = 0;
   py = 0;
   pz = 0;
   e = 0;
   id = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("event_weight", &event_weight, &b_event_weight);
   fChain->SetBranchAddress("Xsec", &Xsec, &b_Xsec);
   fChain->SetBranchAddress("px", &px, &b_px);
   fChain->SetBranchAddress("py", &py, &b_py);
   fChain->SetBranchAddress("pz", &pz, &b_pz);
   fChain->SetBranchAddress("e", &e, &b_e);
   fChain->SetBranchAddress("id", &id, &b_id);
   Notify();
}

Bool_t TreeReader::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void TreeReader::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t TreeReader::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef TreeReader_cxx
