#!/bin/bash
ROOTFLAGS=$(root-config --cflags)
ROOTLD=$(root-config --libs)

PATH_BUILD=${ANALYSIS_BUILD}
PATH_EXE=${ANALYSIS}

mkdir -p ${PATH_BUILD}
mkdir -p ${PATH_EXE}

###########################
#     COMPILE             #
###########################

echo ""
echo ""
echo ""
echo "Compile TreeReader.o:"
g++ -o ${PATH_BUILD}/TreeReader.o -c TreeReader.C TreeReader.h -I  $ROOTFLAGS $FJFLAGS

echo ""
echo ""
echo ""
echo "Compile analyza.o:"
g++ -o ${PATH_BUILD}/Analysis.o -c Analysis.cpp -I  $ROOTFLAGS $FJFLAGS

###########################
#     LINK                #
###########################
echo ""
echo ""
echo ""
echo "Link Analysis:"
g++ -o Analysis ${PATH_BUILD}/Analysis.o $ROOTLD -I  $ROOTFLAGS

echo ""
echo "DONE!"
