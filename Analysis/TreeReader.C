#define TreeReader_cxx
#include "TreeReader.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void TreeReader::Loop()
{
//   In a ROOT session, you can do:
//      root> .L TreeReader.C
//      root> TreeReader t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   // pocet eventu
   Long64_t nentries = fChain->GetEntriesFast();

   // pomocne promene - velikost eventu ...
   // my to nebudeme potrebovat - automaticky se generuje
   Long64_t nbytes = 0, nb = 0;


//  ======================= INIT ==================================

  
  Int_t NumOfEvents = 0;	// id eventu
  int LEPTONOVE=0;		// leptonovy event
  int HADRONOVE=0;		
  int ELEKTRONOVE=0;
  int MIONOVE=0;
  int NEUTRINOVE_E=0;
  int NEUTRINOVE_M=0;
  int NEUTRINOVE_T=0;
  int MALO_CASTIC=0;

  // string - jmeno histogramu
  TString hist_name;

  // pocet binu - histogram pt = sqrt(px*px + py*py)
  const Int_t h_Pt_bins = 100;
  // minimalni pt v histogramu
  Double_t    h_Pt_min = 0.;
  // maximalni pt v histogramu
  Double_t    h_Pt_max = 100.;

  // Podobne pro distrubuci hmotnosti - M
  const Int_t h_M_bins = 120;
  Double_t    h_M_min = 0.;
  Double_t    h_M_max = 120.;

  // Podobne pro distribuci pseudorapidit - eta
  const Int_t h_EtaL_bins = 20;
  Double_t    h_EtaL_min = -2.5;
  Double_t    h_EtaL_max = 2.5;
  

  // jmeno histogramu
  hist_name = "Lep_pt";
  // definuji a inicializuje Histogram TH1D - pt
  TH1D *h_Lep_pt = new TH1D(hist_name,hist_name,h_Pt_bins,h_Pt_min,h_Pt_max);
  // nastav popisek histogramu na x-ose
  h_Lep_pt->SetXTitle("p_{T} [GeV]");
  // nastav popisek histogramu na y-ose
  h_Lep_pt->SetYTitle("events");
  // zohledni i statisticke chyby
  h_Lep_pt->Sumw2();

 
  // Analogitcky pro Eta histogram
  hist_name = "Lep_eta";
  TH1D *h_Lep_eta = new TH1D(hist_name,hist_name,h_EtaL_bins,h_EtaL_min,h_EtaL_max);
  h_Lep_eta->SetXTitle("#eta");
  h_Lep_eta->SetYTitle("events");
  h_Lep_eta->Sumw2(); 

  // Analogicky pro Mmotu
  hist_name = "Pair_m";
  TH1D *h_lep_m = new TH1D(hist_name,hist_name,h_M_bins,h_M_min,h_M_max);
  h_lep_m->SetXTitle("M [GeV]");
  h_lep_m->SetYTitle("events");
  h_lep_m->Sumw2();



   // for smycka pres vsechny eventy v TTree
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      // kontrola ze mohu nacist i-ty event v TTree
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      // nacti event
      nb = fChain->GetEntry(jentry);   nbytes += nb;

      // pricti jednicku k promene celkoveho poctu eventu ... NumOfEvents
      NumOfEvents++;

     //      cout << "\r" << NumOfEvents;

     // budume hledat udalosti Z->mu^- mu^+
     // defenice ctyr-hybnosti pro lepton1 a lepton2
     TLorentzVector lep1, lep2;


     // napocitame kolik mame leptonu:
     int leptonu_v_eventu=0;	// celkovy pocet leptonu v eventu
     int elektrony=0;		// elektrony
     int miony=0;		// muony
     int neutrina_e=0;		// elektronova neutrina
     int neutrina_m=0;		// muionova neutrina
     int neutrina_t=0;		// taonova neutrina
     int id_local=0;
     
     // for smycka pres vsechny castice v eventu
     // i ... i-ta castice
     // id ... vector indentifikacnich indexu v eventu
     for(unsigned int i = 0; i<id->size(); i++) {
       if((*id)[i] != 0) leptonu_v_eventu++;
       if(fabs((*id)[i]) == 11) elektrony++;	// 11 ... id pro elektrony atd...
       if(fabs((*id)[i]) == 12) neutrina_e++;
       if(fabs((*id)[i]) == 13) miony++;
       if(fabs((*id)[i]) == 14) neutrina_m++;
       if(fabs((*id)[i]) == 16) neutrina_t++;
     }

     // pokud nasli alespon 2 leptony v eventu, pak jsme nasli nasi udalost
     // dudeme rozlisovat jednotlive eleptony a pocitat je
     if( leptonu_v_eventu > 1 )
     //v tomto pripade bereme ze Z se rozpadlo leptonove
     {
       LEPTONOVE++;
       // zjisti o jaky lepton se jedna
       if(elektrony>1)  {id_local=11; ELEKTRONOVE++;}
       if(miony>1)      {id_local=13; MIONOVE++;}
       if(neutrina_e>1) {id_local=12; NEUTRINOVE_E++;}
       if(neutrina_m>1) {id_local=14; NEUTRINOVE_M++;}
       if(neutrina_t>1) {id_local=16; NEUTRINOVE_T++;}

	bool first=false;
	bool second=false;
        // for smycka pres vektor slozky_hybnosti_x (vsech castic v udalosti) v nasem leptonovem eventu
	for(unsigned int i=0; i<px->size();i++){
          // nastav ctyr-hybnost prvniho nalezeneho leptonu
	  if(fabs((*id)[i]) == id_local && !first ) {lep1.SetPxPyPzE((*px)[i],(*py)[i],(*pz)[i],(*e)[i]); first=true; continue;}
          // nastav ctyr-hybnost druhehi nalezeneho leptonu
	  if(fabs((*id)[i]) == id_local && !second) {lep2.SetPxPyPzE((*px)[i],(*py)[i],(*pz)[i],(*e)[i]); second=true;}
	}
        // napocitej ctyr-hybnost castice, ktera se rozpadla na ony 2 leptony
        // v nasem prpade MC henerator Herwig generuje udalosti s procesem Z->lepton + lepton
        // timto urcime ctyr-hybnost Z bosonu
 	TLorentzVector pair = lep1 + lep2;
        
        // pridej udalost do histogramu pricnych hybnosti prvniho leptonu
	h_Lep_pt->Fill( lep1.Perp(), 1.0 );
        // Stejne tak pro histogram epseotorapidit ....
	h_Lep_eta->Fill( lep1.Eta(), 1.0 );
	h_Lep_pt->Fill( lep2.Perp(), 1.0 );
	h_Lep_eta->Fill( lep2.Eta(), 1.0 );
	
//cout << pair.E() << "  " << pair.Px() << "  " << pair.Py() << "  " << pair.Pz() << endl; 	
	// urci hmototnust Z bosonu - m^2 = E^2 - p_vector^2
        double m = sqrt( pair.E()*pair.E() - pair.Px()*pair.Px() - pair.Py()*pair.Py() - pair.Pz()*pair.Pz());
//cout << m << endl;	
        // napln histogram hmotnosti z bosonu
	h_lep_m->Fill( m, 1.0 );


     }
     // Pokud se v udalosti neobjevuji alespon dva leptony, pak se jedna o hadronovy rozpad Z bosonu - tzn na quark a anti-quark
     if( leptonu_v_eventu < 2 && id->size() > 1 )
     {
       HADRONOVE++;
   
     }



     // CHECK
     if(leptonu_v_eventu > 3) cout << "VICE LEPTONU"<< endl;
     if(id->size() < 2) MALO_CASTIC++;




   } // end of events loop

   // Summary print 
   cout << "NumOfEvents  " << LEPTONOVE+HADRONOVE << "  LEPTONOVE  " << LEPTONOVE << "  HADRONOVE  "  << HADRONOVE << endl;
   cout << "Neidentifikovane  "  << NumOfEvents-LEPTONOVE-HADRONOVE << endl;
   cout << "MALO_CASTIC  " << MALO_CASTIC << endl;


   // definuj vystupni root-File do ktereho ulozime nase histogramy 
   TFile output_file("output/histograms.root", "RECREATE");
        // uloz histogramy fo root-Filu
	h_Lep_pt->Write();
	h_Lep_eta->Write();
	h_lep_m->Write();

        // ukonci ukldani dat do root-File a uzavri root-File
        output_file.Close();

}
