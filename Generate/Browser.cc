// -*- C++ -*-
//
// This is the implementation of the non-inlined, non-templated member
// functions of the BrowserAnalysis class.
//

#include "Browser.h"
#include "ThePEG/Interface/ClassDocumentation.h"
#include "ThePEG/EventRecord/Particle.h"
#include "ThePEG/Repository/UseRandom.h"
#include "ThePEG/Repository/EventGenerator.h"
#include "ThePEG/Utilities/DescribeClass.h"


//using namespace LHAPDF;
using namespace Mira;
using namespace HepMC;
using namespace Herwig;
using namespace std;
using namespace fastjet;

Browser::Browser() {}

Browser::~Browser() {}



#ifndef LWH_AIAnalysisFactory_H
#ifndef LWH 
#define LWH ThePEGLWH
#endif
#include "ThePEG/Analysis/LWH/AnalysisFactory.h"
#endif

void Browser::analyze(tEventPtr event, long ieve, Int_t loop, Int_t state) {
  AnalysisHandler::analyze(event, ieve, loop, state);
  // Rotate to CMS, extract final state particles and call analyze(particles).


//cout << "here" << endl;

//  Double_t JetLepDRmin = 0.4;
  
  Double_t Rparam4 = 0.6;			//R of the jet 

  event_weight = event->weight();
  Nevt++; 

  CrossSection sigma = generator()->eventHandler()->integratedXSec();
  Xsec = sigma/nanobarn;

  px.clear();
  py.clear();
  pz.clear();
  e.clear();
  id.clear();

 
//==========================================================================
//  Final state - missing ET + jets
//==========================================================================

  tPVector all_final_particles;
//  event->selectFinalState(inserter(all_final_particles));
  all_final_particles = event->getFinalState(); 
 
  vector<PseudoJet> jet_particles;
  vector<PPtr> final_particles;

  double PX_t, PY_t, PZ_t, En_t;

// loop over final state particles - select whatever you want!
  for (tPVector::const_iterator iter = all_final_particles.begin(); iter != all_final_particles.end(); iter++) 
  {
//  particle variables in ThePEG library is:
//  https://thepeg.hepforge.org/doxygen/classThePEG_1_1Particle.html


   if ( ((fabs((*iter)->id()) == 11) || (fabs((*iter)->id()) == 13) ) && (fabs((*iter)->momentum().eta()) < 2.5) && ((*iter)->momentum().perp()/GeV > 10. ) )
   {
     px.push_back((**iter).momentum().x()/GeV);
     py.push_back((**iter).momentum().y()/GeV);
     pz.push_back((**iter).momentum().z()/GeV);
     e.push_back((**iter).momentum().e()/GeV);
     id.push_back((**iter).id());
   }



   if ( ((fabs((*iter)->id()) == 12) || (fabs((*iter)->id()) == 14) || (fabs((*iter)->id()) == 16) ) && (fabs((*iter)->momentum().eta()) < 2.5) && ((*iter)->momentum().perp()/GeV > 20. ) )
   {
     px.push_back((**iter).momentum().x()/GeV);
     py.push_back((**iter).momentum().y()/GeV);
     pz.push_back((**iter).momentum().z()/GeV);
     e.push_back((**iter).momentum().e()/GeV);
     id.push_back((**iter).id());
   }


   if ( ((fabs((*iter)->id()) < 11) || (fabs((*iter)->id()) > 18)) && (fabs((*iter)->momentum().eta()) < 2.5) && ((*iter)->momentum().perp()/GeV > 0.2 ) )

   {
// standard way how to make jets:
      PX_t = (**iter).momentum().x()/GeV;
      PY_t = (**iter).momentum().y()/GeV;
      PZ_t = (**iter).momentum().z()/GeV;
      En_t = (**iter).momentum().t()/GeV;
 
      jet_particles.push_back(PseudoJet(PX_t,PY_t,PZ_t,En_t));

//  ... or we can make jets only from charged particles:
//  https://thepeg.hepforge.org/doxygen/classThePEG_1_1ParticleData.html

//      if ( (**iter).dataPtr()->charged() ) jet_particles.push_back(PseudoJet(PX,PY,PZ,En));
    }
   
  }
  


//  if(final_muons.size() < 2){ cout << "EVENT WITHOUT 2 MUONS!!" << endl; return;}
  RecombinationScheme recomb_scheme = E_scheme;
  fastjet::Strategy strategie = Best;

  JetDefinition jet_def_antiktR4(antikt_algorithm,Rparam4,recomb_scheme,strategie);

  ClusterSequence cs(jet_particles, jet_def_antiktR4);

  vector<PseudoJet> inclusive_jets = cs.inclusive_jets(20.);

  vector<PseudoJet> sorted_jets = sorted_by_pt(inclusive_jets);




// ==================================
// Event selection - we take all events to be saved into trees
// ==================================

 

// TTree filling

  for(UInt_t a=0;a<sorted_jets.size();a++)  {
     px.push_back(sorted_jets[a].px());
     py.push_back(sorted_jets[a].py());
     pz.push_back(sorted_jets[a].pz());
     e.push_back(sorted_jets[a].e());
     id.push_back(0);
  } 



  strom->Fill();

}


LorentzRotation Browser::transform(tcEventPtr event) const {
  return LorentzRotation();
  // Return the Rotation to the frame in which you want to perform the analysis.
}



void Browser::analyze(const tPVector & particles, double weight) {
  AnalysisHandler::analyze(particles);
  // Calls analyze() for each particle.
}


void Browser::analyze(tPPtr, double weight) {}

void Browser::dofinish() {
  AnalysisHandler::dofinish();


//  Scale_histograms();

  file_->Write();
  file_->Close();

}

void Browser::doinitrun() {
  AnalysisHandler::doinitrun();
  // *** ATTENTION *** histogramFactory().registerClient(this); // Initialize histograms.
  // *** ATTENTION *** histogramFactory().mkdirs("/SomeDir"); // Put histograms in specal directory.
 cout << "Beginning" << endl;

  Nevt = 0;
  
  TString hist_name;
//  batch =atoi(gSystem->Getenv("BATCH")); //minimal pT of the jet's leading track
  file_ = TFile::Open("HERWIG_data.root","RECREATE");



  strom = new TTree("strom","strom");
  strom->Branch("event_weight",&event_weight,"event_weight/D");
  strom->Branch("Xsec",&Xsec,"Xsec/D");

  strom->Branch("px",&px);
  strom->Branch("py",&py);
  strom->Branch("pz",&pz);
  strom->Branch("e",&e);
  strom->Branch("id",&id);

}


IBPtr Browser::clone() const {
  return new_ptr(*this);
}

IBPtr Browser::fullclone() const {
  return new_ptr(*this);
}


// If needed, insert default implementations of virtual function defined
// in the InterfacedBase class here (using ThePEG-interfaced-impl in Emacs).



// *** Attention *** The following static variable is needed for the type
// description system in ThePEG. Please check that the template arguments
// are correct (the class and its base class), and that the constructor
// arguments are correct (the class name and the name of the dynamically
// loadable library where the class implementation can be found).
DescribeNoPIOClass<Browser,AnalysisHandler>
  describeMiraBrowser("Mira::Browser", "Browser.so");



void Browser::Init() {

  static ClassDocumentation<Browser> documentation
    ("There is no documentation for the Browser class");

}




/*
bool Browser::isParton(int id)
{
  bool dec = false;
  int partonIDs[9] = {1, 2, 3, 4, 5, 6, 7, 8, 21};
  for (int i=0; i<9; i++) {
    if (fabs(id) == partonIDs[i]) dec=true;
  }
  return dec;
}


bool Browser::isRemnant(int id)
{
  bool dec = false;
  if( (fabs(id) > 1000) && (fabs(id) < 6000) ) dec=true;
 
// remnant is actually PDGID 81 in Herwig

  return dec;
}
*/

vector<PPtr> Browser::sort_particles_pt(vector<PPtr> particles)
{
  Double_t temp_pt1, temp_pt2;
  bool swapped = true;
  Int_t treshold = Int_t(particles.size()-2);
  do{
    swapped = false;
    for (Int_t i = 0; i <= treshold; i++)
    {
      temp_pt1 = particles[i]->momentum().perp()/GeV;
      temp_pt2 = particles[i+1]->momentum().perp()/GeV;
      if( temp_pt1 < temp_pt2 ) 
      { 
        swap(particles[i], particles[i+1] );
        swapped = true;
      }
    }
  } while (swapped == true);
  return particles;
}


vector<tcPPtr> Browser::sort_particles_pt(vector<tcPPtr> particles)
{
  Double_t temp_pt1, temp_pt2;
  bool swapped = true;
  Int_t treshold = Int_t(particles.size()-2);
  do{
    swapped = false;
    for (Int_t i = 0; i <= treshold; i++)
    {
      temp_pt1 = particles[i]->momentum().perp()/GeV;
      temp_pt2 = particles[i+1]->momentum().perp()/GeV;
      if( temp_pt1 < temp_pt2 ) 
      { 
        swap(particles[i], particles[i+1] );
        swapped = true;
      }
    }
  } while (swapped == true);
  return particles;
}


vector<Lorentz5Momentum> Browser::sort_particles_pt(vector<Lorentz5Momentum> particles)
{
  Double_t temp_pt1, temp_pt2;
  bool swapped = true;
  Int_t treshold = Int_t(particles.size()-2);
  do{
    swapped = false;
    for (Int_t i = 0; i <= treshold; i++)
    {
      temp_pt1 = particles[i].perp()/GeV;
      temp_pt2 = particles[i+1].perp()/GeV;
      if( temp_pt1 < temp_pt2 ) 
      { 
        swap(particles[i], particles[i+1] );
        swapped = true;
      }
    }
  } while (swapped == true);
  return particles;
}



Double_t Browser::DeltaR(Double_t eta1, Double_t phi1, Double_t eta2, Double_t phi2){
  Double_t deta = eta1-eta2;
  Double_t dphi = TVector2::Phi_mpi_pi(phi1-phi2);
  
  
  return TMath::Sqrt( deta*deta+dphi*dphi );
}


